const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

const basePath = path.resolve(__dirname, '../');
const APP_DIR = path.resolve(basePath, './src');

module.exports = {
    entry: {
        app: [
            'babel-polyfill',
            `${APP_DIR}/main.js`
        ]
    },
    output: {
        path: path.resolve(basePath, 'dist/'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            { test: /\.js$/, use: ['babel-loader'], include: APP_DIR },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            { test: /\.(png|svg|jpg|gif)$/, use: ['file-loader'] },
            { test: /\.(woff|woff2|eot|ttf|otf)$/, use: ['file-loader'] },
            { test: /\.(ogg|mp3)$/, use: ['file-loader'] }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Production',
            template: './src/index.html'
        }),
        new CleanWebpackPlugin(['dist'], {
            root: `${basePath}`
        }),
        new webpack.DefinePlugin({
            CANVAS_RENDERER: JSON.stringify(true),
            WEBGL_RENDERER: JSON.stringify(true)
        })
    ]
};
