export default function getGameLandscapeDimension(maxWidth, maxHeight) {
    const width = window.innerWidth * window.devicePixelRatio;
    const height = window.innerHeight * window.devicePixelRatio;

    // get the actual w and h. in landscape we'll define w as the longest one
    let landW = Math.max(width, height);
    let landH = Math.min(width, height);

    // do we need to scale to fit in width
    if (landW > maxWidth) {
        const ratioW = maxWidth / landW;
        landW *= ratioW;
        landH *= ratioW;
    }

    // do we need to scale to fit in height
    if (landH > maxHeight) {
        const ratioH = maxWidth / landW;
        landW *= ratioH;
        landH *= ratioH;
    }

    return {
        width: landW,
        height: landH
    };
}
