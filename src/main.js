import Phaser from 'phaser';

import getGameLandscapeDimensions from './utils/scaler';

const PlaceHolderGame = {};
PlaceHolderGame.dim = getGameLandscapeDimensions(800, 600);

PlaceHolderGame.game = new Phaser.Game(
    PlaceHolderGame.dim.width,
    PlaceHolderGame.dim.height,
    Phaser.AUTO
);
